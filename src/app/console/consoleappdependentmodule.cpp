#include "consoleappdependentmodule.h"

#include "subgine/accumulator/module.h"
#include "subgine/animation/module.h"
#include "subgine/aseprite/module.h"
#include "subgine/audio/module.h"
#include "subgine/bag/module.h"
#include "subgine/collision/module.h"
#include "subgine/common/module.h"
#include "subgine/console/module.h"
#include "subgine/entity/module.h"
#include "subgine/event/module.h"
#include "subgine/game/module.h"
#include "subgine/graphic/module.h"
#include "subgine/log/module.h"
#include "subgine/opengl/module.h"
#include "subgine/physic/module.h"
#include "subgine/provider/module.h"
#include "subgine/resource/module.h"
#include "subgine/scene/module.h"
#include "subgine/shape/module.h"
#include "subgine/state/module.h"
#include "subgine/statistic/module.h"
#include "subgine/system/module.h"
#include "subgine/tiled/module.h"
#include "subgine/vector/module.h"
#include "subgine/window/module.h"

#include "course/bomb/module.h"
#include "course/castle/module.h"
#include "course/player/module.h"

using namespace course;

ConsoleApp::DependentModule::DependentModule(kgr::container& container) {
	container.service<sbg::AccumulatorModuleService>();
	container.service<sbg::AnimationModuleService>();
	container.service<sbg::AsepriteModuleService>();
	container.service<sbg::AudioModuleService>();
	container.service<sbg::BagModuleService>();
	container.service<sbg::CollisionModuleService>();
	container.service<sbg::CommonModuleService>();
	container.service<sbg::ConsoleModuleService>();
	container.service<sbg::EntityModuleService>();
	container.service<sbg::EventModuleService>();
	container.service<sbg::GameModuleSerivce>();
	container.service<sbg::GraphicModuleService>();
	container.service<sbg::LogModuleService>();
	container.service<sbg::OpenglModuleService>();
	container.service<sbg::PhysicModuleService>();
	container.service<sbg::ProviderModuleService>();
	container.service<sbg::ResourceModuleService>();
	container.service<sbg::SceneModuleService>();
	container.service<sbg::ShapeModuleService>();
	container.service<sbg::StatisticModuleService>();
	container.service<sbg::StateModuleService>();
	container.service<sbg::SystemModuleService>();
	container.service<sbg::TiledModuleService>();
	container.service<sbg::VectorModuleService>();
	container.service<sbg::WindowModuleService>();
	
	container.service<course::BombModuleService>();
	container.service<course::CastleModuleService>();
	container.service<course::PlayerModuleService>();
}
