#include "service/consoleappservice.h"

int main(int argc, char const** argv) {
	return kgr::container{}.service<course::ConsoleAppService>().run(argc, argv);
}
