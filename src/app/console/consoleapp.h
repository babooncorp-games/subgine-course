#pragma once

#include "subgine/console/console.h"

namespace course {

struct ConsoleApp {
	struct DependentModule;
	
	ConsoleApp(DependentModule&, sbg::Console console);
	
	int run(int argc, char const** argv);
	
private:
	sbg::Console _console;
};

} // namespace course

