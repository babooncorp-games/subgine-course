#include "consoleapp.h"

#include "subgine/resource.h"
#include "subgine/log.h"
#include "subgine/log/logger/filelogger.h"

using namespace course;

ConsoleApp::ConsoleApp(ConsoleApp::DependentModule&, sbg::Console console) : _console{console} {
	sbg::FileLogger::file("console.log");
}

int ConsoleApp::run(int argc, char const** argv) {
	if (argc >= 2) {
		_console.execute(sbg::Console::default_context(argc, argv));
	}
	
	return 0;
}
