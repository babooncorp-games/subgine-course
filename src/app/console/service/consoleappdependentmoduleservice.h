#pragma once

#include "../consoleappdependentmodule.h"

#include "subgine/common/kangaru.h"

namespace course {

struct ConsoleAppDependentModuleService : kgr::single_service<ConsoleApp::DependentModule, kgr::autowire> {};

auto service_map(ConsoleApp::DependentModule) -> ConsoleAppDependentModuleService;

} // namespace subginecourse
