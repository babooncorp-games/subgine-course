#pragma once

#include "../consoleapp.h"

#include "consoleappdependentmoduleservice.h"

#include "subgine/console/service/consoleservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct ConsoleAppService : kgr::single_service<ConsoleApp, kgr::autowire> {};

auto service_map(const ConsoleApp&) -> ConsoleAppService;

} // namespace subginecourse
