#pragma once

#include "consoleapp.h"

#include "subgine/common/kangaru.h"

namespace course {

struct ConsoleApp::DependentModule {
	DependentModule(kgr::container& container);
};

} // namespace course
