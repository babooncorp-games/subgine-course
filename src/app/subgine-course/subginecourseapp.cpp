#include "subginecourseapp.h"

#include "subgine/resource.h"
#include "subgine/window.h"
#include "subgine/scene.h"
#include "subgine/game.h"

namespace course {

SubgineCourseApp::SubgineCourseApp(DependentModule&, sbg::Window& window, sbg::Game game, sbg::SceneManager& scenes, sbg::GameDatabase& database) noexcept :
	_window{window},
	_game{game},
	_scenes{scenes},
	_database{database} {}

int SubgineCourseApp::run(int argc, char** argv) {
	_database.load("res/course/scene.cbor");
	
	_window.open(sbg::WindowSettings{"Subgine Course", {1366, 768}, false});
	auto&& scene = _scenes.create("course-castle");
	
	scene.load();
	scene.plug();
	
	_game.run();
	
	return 0;
}

} // namespace course
