#include "service/subginecourseappservice.h"

int main(int argc, char** argv) {
	return kgr::container{}.service<course::SubgineCourseAppService>().run(argc, argv);
}
