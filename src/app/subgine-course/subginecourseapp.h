#pragma once

#include "subgine/game/game.h"

namespace sbg {
	struct SceneManager;
	struct Window;
	struct SceneManager;
	struct GameDatabase;
}

namespace course {

struct SubgineCourseApp {
	struct DependentModule;
	
	SubgineCourseApp(DependentModule&, sbg::Window& window, sbg::Game game, sbg::SceneManager& scenes, sbg::GameDatabase& database) noexcept;
	
	int run(int argc, char** argv);
	
private:
	sbg::Window& _window;
	sbg::Game _game;
	sbg::SceneManager& _scenes;
	sbg::GameDatabase& _database;
};

} // namespace course
