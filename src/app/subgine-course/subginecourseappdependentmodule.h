#pragma once

#include "subginecourseapp.h"

#include "subgine/common/kangaru.h"

namespace course {

struct SubgineCourseApp::DependentModule {
	DependentModule(kgr::container& container);
};

} // namespace course
