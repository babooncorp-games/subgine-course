#pragma once

#include "../subginecourseapp.h"

#include "subginecourseappdependentmoduleservice.h"

#include "subgine/window/service/windowservice.h"
#include "subgine/scene/service/scenemanagerservice.h"
#include "subgine/game/service/gameservice.h"
#include "subgine/resource/service/gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct SubgineCourseAppService : kgr::single_service<SubgineCourseApp, kgr::autowire> {};

auto service_map(const SubgineCourseApp&) -> SubgineCourseAppService;

} // namespace subginecourse
