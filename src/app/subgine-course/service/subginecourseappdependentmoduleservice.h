#pragma once

#include "../subginecourseappdependentmodule.h"

#include "subgine/common/kangaru.h"

namespace course {

struct SubgineCourseAppDependentModuleService : kgr::single_service<SubgineCourseApp::DependentModule, kgr::autowire> {};

auto service_map(SubgineCourseApp::DependentModule) -> SubgineCourseAppDependentModuleService;

} // namespace subginecourse
