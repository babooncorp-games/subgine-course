#pragma once

#include "../controls.h"

#include "subgine/window/service/inputtrackerservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct ControlsService : kgr::single_service<Controls, kgr::autowire> {};

auto service_map(Controls const&) -> ControlsService;

} // namespace course
