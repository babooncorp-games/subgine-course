#pragma once

#include "../playermodule.h"

#include "subgine/event/service/actorcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/resource/service/resourcedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct PlayerModuleService : kgr::single_service<PlayerModule>,
	sbg::autocall<
		&PlayerModule::setupActorCreator,
		&PlayerModule::setupEntityBindingCreator,
		&PlayerModule::setupPluggerCreator,
		&PlayerModule::setupResourceDatabase
	> {};

auto service_map(const PlayerModule&) -> PlayerModuleService;

} // namespace course
