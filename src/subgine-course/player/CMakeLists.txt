add_sbg_module(subgine-course::player
SOURCES
	action/attackaction.cpp
	controls.cpp
	
DEPENDENCIES
	sbg::entity
	sbg::event
	sbg::log
	sbg::window
	
MODULE_CLASS_DEPENDENCIES
	sbg::system
	sbg::scene
	sbg::collision
	sbg::opengl
	sbg::physic
	sbg::resource
)
