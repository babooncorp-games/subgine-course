#include "playermodule.h"

#include "course/player.h"
#include "course/player/service.h"

#include "subgine/entity.h"
#include "subgine/event.h"
#include "subgine/log.h"
#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/scene.h"

#include "subgine/entity/service.h"
#include "subgine/event/service.h"

using namespace course;

void PlayerModule::setupActorCreator(sbg::ActorCreator& actorCreator) const {
	
}

void PlayerModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("block-controls", [](/*Controls& controls, */sbg::Entity entity) {
		// We need to control `entity`. It's our main character
	});
}

void PlayerModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("course-controls", [](sbg::EventDispatcher& events, Controls& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}

void PlayerModule::setupResourceDatabase(sbg::ResourceDatabase& resourceDatabase) const {
	resourceDatabase.fromConfig(location, "asset/asset.json");
}
