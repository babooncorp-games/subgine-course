#pragma once

#include "subgine/resource/modulelocation.h"

namespace sbg {
	struct ActorCreator;
	struct EntityBindingCreator;
	struct PluggerCreator;
	struct ResourceDatabase;
}

namespace course {

struct PlayerModule {
	void setupActorCreator(sbg::ActorCreator& actorCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
	void setupResourceDatabase(sbg::ResourceDatabase& database) const;
	
	static const sbg::ModuleLocation location;
};

} // namespace course
