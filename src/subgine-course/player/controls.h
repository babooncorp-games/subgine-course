#pragma once

#include "subgine/window/inputtracker.h"
#include "subgine/entity/entity.h"
#include "subgine/event/keyboardevent.h"

namespace course {

struct Controls {
	Controls(sbg::InputTracker& tracker);
	
	// Setter for the member `target`
	void control(sbg::Entity entity);
	
	// Handles inputs
	void handle(const sbg::KeyboardEvent& event);
	
private:
	sbg::Entity _target;
	sbg::InputTracker& _tracker;
};

} // namespace course
