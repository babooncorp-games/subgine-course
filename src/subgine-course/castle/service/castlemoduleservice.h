#pragma once

#include "../castlemodule.h"

#include "subgine/resource/service/resourcedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct CastleModuleService : kgr::single_service<CastleModule>,
	sbg::autocall<
		&CastleModule::setupResourceDatabase
	> {};

auto service_map(CastleModule const&) -> CastleModuleService;

} // namespace course
