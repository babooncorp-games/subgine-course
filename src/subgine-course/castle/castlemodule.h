#pragma once

#include "subgine/resource/modulelocation.h"

namespace sbg {
	struct ResourceDatabase;
}

namespace course {

struct CastleModule {
	void setupResourceDatabase(sbg::ResourceDatabase& database) const;
	
	static const sbg::ModuleLocation location;
};

} // namespace course
