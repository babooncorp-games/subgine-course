#include "castlemodule.h"

#include "subgine/resource.h"

using namespace course;

void CastleModule::setupResourceDatabase(sbg::ResourceDatabase& resourceDatabase) const {
	resourceDatabase.fromConfig(location, "asset/asset.json");
}
