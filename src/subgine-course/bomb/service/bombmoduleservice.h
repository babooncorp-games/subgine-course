#pragma once

#include "../bombmodule.h"

#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"
#include "subgine/graphic/service/paintercreatorservice.h"
#include "subgine/resource/service/resourcedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct BombModuleService : kgr::single_service<BombModule>,
	sbg::autocall<
		&BombModule::setupEntityBindingCreator,
		&BombModule::setupCollisionReactorCreator,
		&BombModule::setupModelCreator,
		&BombModule::setupPainterCreator,
		&BombModule::setupResourceDatabase
	> {};

auto service_map(const BombModule&) -> BombModuleService;

} // namespace course
