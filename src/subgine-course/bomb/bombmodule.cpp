#include "bombmodule.h"

#include "course/bomb.h"
#include "course/bomb/service.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/log.h"
#include "subgine/opengl.h"
#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/system.h"

#include "subgine/opengl/service.h"
#include "subgine/system/service.h"

#include <chrono>

using namespace course;
using namespace std::literals;

void BombModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const {
	collisionReactorCreator.add("press", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity) {
		return [&scheduler, entity](sbg::BasicCollisionInfo) {
			auto& visual = entity.component<sbg::Visual>();
			
			// Each frame that a collision happen, increment sprite.pressed
			visual.visit("button", [](FrameSprite& sprite) {
				sprite.frame = 1;
				sprite.pressed++;
			});
			
			auto pressed = visual.inspect<FrameSprite>("button").pressed;
			
			scheduler.defer(50ms, [&visual, entity, pressed] {
				if (entity) {
					visual.visit("button", [&](FrameSprite& sprite) {
						// if sprite.press is the same value as captured, no collision happened in 50ms.
						if (pressed == sprite.pressed) {
							sprite.frame = 0;
						}
					});
				}
			});
		};
	});
}

void BombModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	
} 

void BombModule::setupModelCreator(sbg::ModelCreator& modelCreator) const {
	modelCreator.add("manual-frame-sprite", [](sbg::Entity entity, sbg::Property data) {
		FrameSprite sprite;
		
		if (entity.has<sbg::ZValue>() && entity.has<sbg::PhysicPoint2D>()) {
			auto&& physic = entity.component<sbg::PhysicPoint2D>();
			auto&& zValue = entity.component<sbg::ZValue>();
			
			sprite.position = [&physic, &zValue] {
				auto position = physic.getPosition();
				return sbg::Vector3d{position.x, position.y, zValue};
			};
		} else if (entity.has<sbg::PhysicPoint3D>()) {
			sprite.position = makePositionProvider(entity.component<sbg::PhysicPoint3D>());
		}
		
		sprite.size = sbg::parseVector<sbg::Vector2f>(data["size"]);
		sprite.texture = data["texture"].asString();
		
		return sprite;
	});
}

void BombModule::setupPainterCreator(sbg::PainterCreator& painterCreator) const {
	painterCreator.add("manual-frame-sprite-painter", [](
		kgr::generator<sbg::opengl::SpritePainterService<FrameSprite>> makePainter,
		sbg::ProgramManager programManager,
		sbg::SharedArrayMeshFactory<sbg::Vertex<sbg::Vector3f, sbg::Vector2f>> meshFactory,
		sbg::Property data
	) {
		auto l = sbg::Log::debug(SBG_LOG_INFO, "Creating frame painter");
		return makePainter(programManager.get(data["program"]), meshFactory.create(data["mesh"]));
	});
}

void BombModule::setupResourceDatabase(sbg::ResourceDatabase& resourceDatabase) const {
	resourceDatabase.fromConfig(location, "asset/asset.json");
}
