#pragma once

#include "subgine/entity/entity.h"
#include "subgine/audio/factory/soundbufferfactory.h"

#include <vector>

namespace sbg {
	struct AudioManager;
}

namespace course {

struct BombDetonator {
	BombDetonator(sbg::SoundBufferManager soundBufferManager, sbg::AudioManager& audioManager);
	
	void detonate();
	
private:
	sbg::SoundBufferManager _soundBufferManager;
	sbg::AudioManager& _audioManager;
};

} // namespace course
