#pragma once

#include "subgine/graphic/model/simplesprite.h"

namespace course {

struct FrameSprite : sbg::SimpleSprite {
	int frame = 0;
	unsigned int pressed = 0;
	
	static constexpr auto uniforms() {
		return sbg::uniforms(
			SimpleSprite::uniforms(),
			sbg::uniform::updating("frame", &FrameSprite::frame)
		);
	}
};

} // namespace course
