#include "bombdetonator.h"

#include "subgine/audio/audiosystem.h"

#include "subgine/log.h"

using namespace course;

BombDetonator::BombDetonator(sbg::SoundBufferManager soundBufferManager, sbg::AudioManager& audioManager) :
	_soundBufferManager{std::move(soundBufferManager)},
	_audioManager{audioManager} {}

void BombDetonator::detonate() {
	// Only if there is bombs to explode
	auto l = sbg::Log::trace(SBG_LOG_INFO, "Detonating bombs");
// 	_audioManager.play(sbg::Sound{_soundBufferManager.get("explosion1")});
}
