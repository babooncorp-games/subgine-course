#pragma once

#include "subgine/resource/modulelocation.h"

namespace sbg {
	struct CollisionReactorCreator;
	struct EntityBindingCreator;
	struct ModelCreator;
	struct PainterCreator;
	struct ResourceDatabase;
}

namespace course {

struct BombModule {
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupModelCreator(sbg::ModelCreator& modelCreator) const;
	void setupPainterCreator(sbg::PainterCreator& painterCreator) const;
	void setupResourceDatabase(sbg::ResourceDatabase& database) const;
	
	static const sbg::ModuleLocation location;
};

} // namespace course
