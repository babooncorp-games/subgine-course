# Bienvenue au Tutoriel

Donc vous avez eu le cours théorique? Prêt à se sallir les mains? Parfait!

Dans ce tutoriel, nous allons implémenter ensemble des méchanique dans un jeu à completer.

Le déroulement du tutoriel est divisé en étapes:

 1. Vue d'Ensemble
 2. Les Déplacements
 3. Création d'Entités
 4. Les Systèmes

Bonne chance!

## Vue d'Ensemble

Il y a deux dossiers principaux à un projet subgine: `src/` et `res/`. Il contiennent respectivement le code source, et les ressources (assets) du jeu.

Dans le dossier de ressources, il y a en premier les base de données json. Il contiennent les listes des assets et les descriptions d'entités.

Par exemple, dans `entity.json`, il y a la liste de tous les types d'entités instantiables. Par exemple, le premier entité qui s'y trouve est `"happy-block-green"`, l'entité que le joueur contrôlera dans ce jeu.

### Json

Notre format json se définit comme tel: chaque entrées d'une base de donné (comme `entity.json`) commence par un objet root qui contient un objet ayant le nom de la base de donnée.

Voici la structure de `entity.json`:

```
{
    "entity": {
        "type1": {
            ...
        },
        "type3": {
            ...
        },
        "type2": {
            ...
        }
    }
}
```

Chaque type d'entité a sa liste de components et sa liste de bindings. Le component sont un unité de données et/ou de logique que l'entité devra possèder. Un binding représente quel systèmes cet entité devra faire partie.

Les autres sous-dossiers contiennent les assets du jeu.

### Sources

Les sources sont divisées en deux. Les applications et les modules. Les modules sont des unité de code indépendant qui implémente des système ou des éléments de gameplay.

Un application fournis toujours un main. Ce main ressemblera toujours a ceci:

```
#include "service/subginecourseappservice.h"

int main(int argc, char** argv) {
    return kgr::container{}.service<course::SubgineCourseAppService>().run(argc, argv);
}
```

La fonction `run` va créer la scène de départ et lancer la boucle principale du jeu. Voici un extrait de `subginecourseapp.cpp`

```
int SubgineCourseApp::run(int argc, char** argv) {
    _window.open(sbg::WindowSettings{"Subgine Course", {1366, 768}, false});
    auto&& scene = _scenes.create("course-castle");
    
    scene.load();
    scene.plug();
    
    _game.run();
    
    return 0;
}
```

Pour plus d'information sur la procédure de lancement d'un application, allez voir la page du wiki [Main](http://gufide.mooo.com:8080/w/subgine/concepts/flux/).

Vous verrez aussi une classe bien étrange nommé comme l'application suivi de "DependentModule". Ceci est une classe qui se charge d'instantier les modules de l'application dans l'état global.

Si vous ouvrez ce fichier, vous allez y voir tous les modules de subgine ainsi que les module du jeu:

![folder-and-module-instantiation](img/modules-folder.png)

Comme vous pouvez le voir dans cet image, il y a à gauche deux répertoire `player` et `bomb`. Ce sont des module du jeu. À droite, on peut voir ces modules se faire instantier.

_Allons voir ces modules de plus près!_

Un module est composé de trois éléments éssentiels: La classe module, la définition de service de la classe module, et les include modulaires.

La classe module se charge de configurer l'état global du jeu afin de "brancher" les classe du module avec les autres. Ensuite, il y a la définition de service de la classe module. La définition de service est une configuration pour savoir comment instantier le module. Enfin, les include modulaires dans le dossier `include/`. La grande partie du travail se fera dans la classe module de course, soit les fichiers `playermodule.cpp` et `bombmodule.cpp`.

Une documentation étendue se trouve sur le wiki, à la page [Modules](http://gufide.mooo.com:8080/w/subgine/module/).

 * Le module `player` implémente toutes les classes reliée au controles et au joueur.
 * Le module `bomb` implémente les bombes et les fonctionalitées reliées.

### Classe Module

Il y a une classe module qui contient beaucoup d'information pertinentes. Dans la classe module du module course, on peux y voir plusieurs fonctions. chacune de ces fonctions ajoute des nouveau _builder_ dans des _creator_ variés. Allons voir ce qui se trouve dans `bombmodule.cpp`.

Comme vous pouvez remaquer, tous les fonctions commence par `setup`. C'est normal car tous les action que la classe module fait dans ce module est d'ajouter des builders.

Chacunes de fonction _setup_ ajoute des builders. Ces builders sont différent pour chacun des _Creator_ dans lesquels on les ajoutes. Par exemple, un builder pour le `CollisionReactorCreator` dois retourner un `CollisionReactor`, tandis que un builder pour le `ActorCreator` dois retourner des `Subscriber`. Les _Creator_ n'accepterons pas le builder tant qu'il ne réponde pas au critère requis par ce même _Creator_.

C'est tout pour la vue d'ensemble! Ceci n'est pas une documentation complète d'un jeu de base, mais simplement un introduction rapide à la structure d'un projet. Dans la prochaine section, nous allons commencer a implémenter des fonctionalités dans le jeu.

> NOTE: Pour savoir la responsabilité qu'une des fonctions setup accompli, veuillez visiter l'annexe à la fin du document.

## Les Déplacements

Allons voir la classe `Controls`, dans `controls.h`. Il y a la fonction `control(entity)` qui est un setter pour l'entité à controler. Il y a aussi `handle(event)`, qui traite les inputs.

Pour faire en sorte que le joueur soit envoyé au contrôles, il faut un binding dans `entity.json` et implémenter la fonction qui _bind_ l'entité au setter.

Par chance, le binding est déjà présent:

![block-control-builder](img/block-controls-builder.png)

Puisque le binding est présent, l'engin va chercher la fonction `"block-controls"` dans le `sbg::EntityBindingCreator`.

Cette fonction est déjà présente dans le tutoriel, mais elle est vide. Elle se trouve dans le fichier `playermodule.cpp`, à la ligne 22:

![binding-creator-controls](img/binding-creator-controls.png)

Pour faire fonctionner le tout, implémentez le contenu du builder. Vous devrez envoyer l'entité du joueur au controles en vous servant du setter.

_Indice: vous pouvez décommenter le paramètre `controls` afin de le recevoir_

### Activer le saut

Nous voulons aussi que notre joyeux carré vert puisse sauter. Si vous inspectez `controls.cpp`, vous verrez que le `JumpAction` est déjà envoyé a l'acteur.

Il y a un type de subscriber pour le saut déjà fourni dans subgine. Ce type est `"jump2D"`. Ajoutez cette réponse d'action au `"happy-block-green"` dans `entity.json`.

Comme data, inscrivez une force proche de 300:

```
"strength": 300
```

_Comment savoir quel data inscrire quand on crée des objets dans le json?_

Le seul moyen pour l'instant est d'aller voir l'implémentation du builder. Pour le `jump2D`, l'implémentation se trouve dans le fichier `subgine/event/eventmodule.cpp` à la ligne 13.

## Création d'Entités

Afin que le jeu aie plus d'intéractions, nous allons ajouter l'abilité de déposer des bombes. Pour cela, nous allons créer un tous nouveau type d'action et créer des entité avec le `sbg::EntityFactory`.

### Action d'Attaque

Nous avons prévu que la touche `X` sera l'action d'attaque général a travers le gameplay du jeu. Afin que la touche `X` aie un effet, nous devrons changer le code de `Controls::handle(event)`. L'action de type `AttackAction` devra être envoyée à l'acteur principal. Pour envoyer ce type d'évenement, il suffira d'envoyer un instance de `AttackAction` à la fonction `actor.act`:

![controls](img/controls.png)

Compilez et observez le résultat. Par malheur, rien ne se produit lorsqu'on appuie `X`. Même si on envoie l'action a l'acteur, l'entité controlée ne possède aucune fonctions qui répond a cette action.

Jusqu'a présent, l'engin ne connais pas comment créer un acteur qui écoute l'action `AttackAction` et ne sais pas non plus quel code executer lorsqu'un tel action se produit. Nous allons devoir ajouter un nouveau builder au `ActorCreator`.

Dans le fichier `playermodule.cpp` à la ligne 18, vous avez accès à l'instance `actorCreator`, reçu par référence. Servez vous de cette variable afin de lui ajouter un nouveau builder.

Un petit rappel pour créer un builder: la fonction `creator.add(...)` prends deux paramètre: le nom du builder et une fonction retournant l'objet requis (le builder). Dans notre cas, nous allons appeller notre builder `"attack-projectile"`. Ensuite, le builder recevera un `sbg::Entity` et un `sbg::Property`.

Ensuite, afin que votre builder soit valide pour le créateur d'acteur, il dois retourner un subscriber. Vous pouvez utiliser le _logger_ afin de valider que l'action est bien exécutée.

![actor-creator](img/actor-creator.png)

Quand vous aurez terminer d'écrire le subscriber, vous devrez modifier l'entité du joueur afin qu'il possède votre nouveau subscriber. Dans `entity.json`, il y a une liste de subscriber pour l'acteur du personnage `"happy-block-green"`. Dans cette liste de subscriber, ajoutez votre type de subscriber en utilisant le nom de votre builder, soit `"attack-projectile"`. Comme data, ajoutez la propriété `"default-projectile"` avec la valeur `"happy-bomb"`. Le string `"happy-bomb"` est le data obtenu par l'expression `data["default-projectile"].asString()` dans le C++.

### Déposer des Bombes

Pour déposer des bombes, il faut créer des entité. Cela va se faire a travers le `sbg::EntityFactory`. Nous allons modifier notre builder `"attack-projectile"` que nous avons créer précédemment. Afin de rendre le factory disponible, ajoutez `sbg::EntityFactory factory` comme __premier__ paramètre au builder. Le `sbg::EntityFactory` est un service non partagé, il peut donc être injecté comme paramètre et ne dois pas être reçu par référence.

Dans ce subscriber, vous devrez créer un projectile du type contenu dans la propriété spécifiée dans le json. la variable `data` agit comme un dictionnaire, on peut donc accéder à `data["default-projectile"].asString()`.

Petit rappel: le factory a une fonction `create(...)` qui prends un string en paramètre. Vous pouvez vous servir des variables qui on été prédéfinies dans la fonction.

Afin que le résultat soit réaliste, l'entité créé devra être à la même position que le joueur. La fonction `physic.getPosition()` retournera la position actuelle du joueur. Servez vous de la fonction `setPosition(...)` de l'entité nouvellement créé.

Testez et admirez le résultat.

> BONUS: Tentez d'affecter la velocité du projectile à la moité de la vélocité du joueur.

### Plusieurs Types de Bombes

Dans cette partie nous allons diversifier nos projetiles et allons voir comment un action peut transférer de l'information à l'acteur. En ce moment, in créé seulement le projectile contenu dans le json: `"default-projectile": "happy-bomb"`. Ajoutons un moyen de spécifier un autre type que le default!

Dans le fichier `playermodule/action/attackaction.h`, Vous verrez la definition du type de l'action d'attaque. Ajoutez un membre de type string optionel, comme ceci: `std::optional<std::string> projectile = {}`.

Ensuite, rendez vous dans `controls.cpp` et ajoutez une nouvelle action quand on appuie sur `C`, et envoyez le string `"big-bomb"` en paramètre.

Enfin, dans `playermodule.cpp`, créez l'entité spécifié dans l'action si le string est assigné. Allez voir des exemples sur [`std::optional`](https://en.cppreference.com/w/cpp/utility/optional).

## Les Systèmes

Des bombes sans explosions, c'est pas marrant! Nous allons implémenter les méchanique des bombes dans le module de bombes.

Nous allons implémenter une méchanique de gameplay à l'aide d'un système. Un système est toute classe qui intéragie avec un ou plusieurs entités afin d'implémenter une méchanique de jeu. Une ébauche de système existe déjà dans le fichier `bombdetonator.h` et `bombdetonator.cpp`.

L'objectif est que lorsque le joueur appuie sur le bouton se trouvant a gauche du niveau, tous les bombe sur la carte explose.

Il faudra donc modifier la class `BombDetonator` afin qu'elle contienne une liste d'entités. Il faudra aussi qu'à la détonation, tous les bombe soient détruites et la liste vidée. Comme type de collection, utilisez un `std::vector` contenant des `sbg::Entity`. Ajoutez une fonction `add(entity)` qui ajoutera un d'entité à la collection.

Ensuite, vous devez compléter la fonction `detonate()`. Il faudra qu'elle s'active seulement si il y a des bombes. Elle s'occupera de détruire tous les entités de bombes et jouer un son d'explosion. Attention! N'oubliez pas dans `detonate()` de vider le vecteur d'entité après les avoir détruits! Utilisez la fonction `vector.clear()`.

> HINT: La classe `sbg::Entity` possède la fonction `destroy()` qui détruit l'entité cible.

Pour qu'une bombe soit ajoutée au detonator, nous devons créer un nouveau _binding_, comme nous avons fait pour lier le joueur au contrôles. Pour cela, vous devez ajouter un nouveau type de binding dans le `sbg::EntityBindingCreator`, à la ligne 51 de `bombmodule.cpp`. __N'oubliez pas__ d'ajouter ce nouveau binding dans la liste des bindings de l'entité de type `"happy-bomb"` _et_ `"big-bomb"`, dans le `entity.json`.

> HINT: La classe `BombDetonator` est un service partagé, vous pouvez donc le recevoir par référence en premier paramètre dans votre builder.

### Ajout des Réacteur de Collisions

Afin que la détonation se produise lors de l'activation du bouton, il faudra ajouter un réacteur de collision. Il y a déjà un réacteur de collision dans à la ligne 39 du fichier `coursemodule.cpp`.

Pour que le réacteur de collision soit ajouté au bouton, allez dans le fichier `collisionbody.json`. Vous verrez un entrée nommé `"course-button"`. Dans cet entrée il y a déjà un réacteur contre les `"blocks"` qui active la fonction `"press"`. Ajoutez votre réacteur de collision.

> BONUS: Activez le réacteur `"reciprocal2D"` afin que le bloc vert entre en collision avec le bloc rose.

# Annexe

Voici une description de chacune des fonction setup de `playermodule.cpp`:

 - La première fonction est `setupActorCreator`. Cette fonction ajoutera des builder d'actions au créateur d'acteurs. Elle attache un nom comme `block-attack-projectile` à une fonction qui retourne un subscriber à insérer dans un acteur. Le type d'arugment que le listener prends en paramètre démontre le type d'action que ce subscriber écoutera. Nous nous servirons de cette fonction afin d'ajouter des nouveau type d'action que le joueur pourra exécuter lors d'un input.
 - La deuxième fonction est `setupEntityBindingCreator`. Cette fonction ajoute des nouveau type de _binding_ qu'un entité sera envoyé. Par exemple, si nous avons un système de course qui contient tous les entité qui représente un courseur, on ajouterais un binding de type `"runner"` qui permetterrait a tous courseur de s'ajouter au système de course. Nous nous servirons de cette fonction afin d'ajouter des éléments de gameplay qui dois contenir un ou des entités précis.
 - La troisième fonction, nommée `"setupPluggerCreator"`, ajoute des nouvelle manière qu'une scène peut se brancher dans l'état global de l'engin. Si une scène n'est pas branchée, elle n'aura aucun effet sur le jeu! C'est pour cela qu'au branchement, nous voulons ajouter la classe `"Controls"` a la liste des abonnés au input de la fenêtre. Dans le cas contraire, quand on débranche la scène, on s'assure de déabonner les contrôles. Comme les deux dernière fonction, nous aurons pas à la modifier au cours de ce tutoriel.

Voici une description de chacune des fonction setup de `bombmodule.cpp`:

 - La première fonction est `setupCollisionReactorCreator`. Cette fonction attache un nom comme `press` à une fonction qui retourne un `CollisionReactor`. Nous nous servirons de cette fonction afin d'ajouter des nouvelle façon de réagir a un collision.
 - La deuxième fonction est `setupEntityBindingCreator`. Tout comme dans le `playermodule.cpp`, on achemine les entité au endroits requis. Dans notre cas, les bombes dans le détonateur.
 - La troisième fonction est `setupModelCreator`. Cette fonction ajoute un nouveau type de _model_ qu'un entité peut posséder. Dans subgine, un model est une structure simple comportant tous les données requise afin de dessiner l'objet en question. On peut observer l'ajout de `"manual-frame-sprite"`, qui est une sprite animée qu'on peut manuellement changer la frame affichée. Dans notre cas, nous affichons le bouton enfoncé quand on l'appui. Nous aurons pas a modifier cette fonction dans ce tutoriel.
 - La quatrieme fonction est `setupPainterCreator`. C'est dans cette fonction qu'on ajoute des nouveau type de painter. Dans notre cas, nous voulons afficher le type de sprite retourné par `"manual-frame-sprite"`. Comme la dernière, nous aurons pas a modifier la fonction.
